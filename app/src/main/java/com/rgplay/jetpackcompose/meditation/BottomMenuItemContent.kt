package com.rgplay.jetpackcompose.meditation

import androidx.annotation.DrawableRes

data class BottomMenuItemContent(
    val title: String,
    @DrawableRes val iconId: Int
)
