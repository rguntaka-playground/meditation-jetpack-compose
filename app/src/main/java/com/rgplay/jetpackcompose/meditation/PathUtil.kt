package com.rgplay.jetpackcompose.meditation

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Path
import kotlin.math.abs

/*
 Util function to calculate (x1,y1) and (x2,y2)
 */
fun Path.standardQuadFromTo(from:Offset, to:Offset) {
    quadraticBezierTo(
        from.x,
        from.y,
        abs(from.x+to.x)/2f,
        abs(from.y+to.y)/2f
    )
}